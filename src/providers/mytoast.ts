import { ToastController, Toast } from 'ionic-angular';
import { Injectable } from '@angular/core';

/*
  Generated class for the Mytoast provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MyToast {

  constructor(private toastCtrl: ToastController) {
  }

   public showToaster(_message: string, duration?: number): Toast {
    let toast = this.toastCtrl.create({
      message: _message,
      duration: duration ? duration : null,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
    return toast;
  }

}
