import { UserLogin } from './../models/userLogin';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { server } from './../enviromment/enviromment';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the VisitsService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class VisitsService {

  constructor(public http: Http) {
    console.log('Hello VisitsService Provider');
    
  }

  getVisits(userLogin: UserLogin): Observable<any> {
    return this.http.get(`${server}/installer/${userLogin.installer._id}/visits`, {
      headers: this.getAuth(userLogin.token)
    });
  }

  createVisit(userLogin: UserLogin, visit): Observable<any> {
    visit.installer = userLogin.installer._id;
    return this.http.post(`${server}/installer/${userLogin.installer._id}/visits`, visit, 
    {
      headers: this.getAuth(userLogin.token)
    });
  }

  getAuth(token): Headers {
    const header = new Headers()
    header.append('Authorization', token)
    return header;
  }

}
