import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { server } from './../enviromment/enviromment';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the InstallersService provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class InstallersService {

  constructor(public http: Http) {
    console.log('Hello InstallersService Provider');
  }

  getInstallersByName(name: string): Observable<any> {
    return this.http.get(`${server}/installerfp?name=${name}`);
  }

  register(installer: any): Observable<any> {
    return this.http.post(`${server}/installer`, installer);
  }

}
