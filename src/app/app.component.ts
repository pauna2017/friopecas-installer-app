import { TabsPage } from './../pages/tabs/tabs';
import { AuthService } from './../providers/auth-service';
import { LoginPage } from './../pages/login/login';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  @ViewChild(Nav) nav : Nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
  private authService: AuthService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.backgroundColorByHexString('#e7930e');
      splashScreen.hide();
    });
    this.checkLogin();
  }

  checkLogin(): void {
    this.authService.getToken()
      .then(data => {
        if(data && data.token){
          this.nav.setRoot(TabsPage);
        }
      })
      .catch(error => {
        console.log('error', error);
      })
  }
}
