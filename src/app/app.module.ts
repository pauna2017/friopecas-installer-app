import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';

import { LoginPage } from './../pages/login/login';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { AccountPage } from './../pages/account/account';
import { FinishSignupPage } from './../pages/finish-signup/finish-signup';
import { SignupPage } from './../pages/signup/signup';
import { NewVisitPage } from './../pages/new-visit/new-visit';
import { ChatPage } from './../pages/chat/chat';

import { NativeStorageMock } from './../mock/NativeStorageMock';
import { NativeStorage } from '@ionic-native/native-storage';
import { LoginService } from './../providers/login-service';
import { AuthService } from './../providers/auth-service';
import { VisitsService } from './../providers/visits-service';
import { InstallersService } from './../providers/installers-service';
import { MyToast } from './../providers/mytoast';

import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Camera } from '@ionic-native/camera';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    FinishSignupPage,
    AccountPage,
    NewVisitPage,
    ChatPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    SignupPage,
    FinishSignupPage,
    AccountPage,
    NewVisitPage,
    ChatPage
  ],
  providers: [
    Camera,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {provide: NativeStorage, useClass: NativeStorageMock},
    AuthService,
    LoginService,
    InstallersService,
    MyToast,
    VisitsService,
    LaunchNavigator
  ]
})
export class AppModule {}
