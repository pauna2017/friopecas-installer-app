import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';

import { UserLogin } from './../../models/userLogin';
import { LoginService } from './../../providers/login-service';
import { AuthService } from './../../providers/auth-service';
import { MyToast } from './../../providers/mytoast';

import { Credentials } from './../../models/credentials';
import { SignupPage } from './../signup/signup';
import { TabsPage } from './../tabs/tabs';

import 'rxjs/add/operator/finally';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  public credential: Credentials = {
    email: '',
    password: ''
  };

  constructor(public navCtrl: NavController,
   public navParams: NavParams,
   private myToast: MyToast,
   private loadingCtrl: LoadingController,
   private loginService: LoginService,
   private authService: AuthService,
   private alertCtrl: AlertController) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  public login(form, invalidEmail): void {
    if(invalidEmail){
      this.myToast.showToaster('Email inválido.', 2000);
      return;
    }
    if(!form.valid){
      this.myToast.showToaster('Email e senha são obrigatórios.', 2000);
      return;
    }

    const loader:Loading = this.presentLoading();
    this.loginService.login(this.credential)  
      .finally(() => {
        loader.dismiss();
      })    
      .map(data => <UserLogin> data.json())
      .subscribe(
        data => {          
          if(data['client']){
            this.myToast.showToaster('Clientes devem acessar somente o aplicativo de clientes. Este aplicativo é restrito para os instaladores FrioPeças.');
            return;
          } 
          console.log('data', data);
          this.authService.login(data);
          this.navCtrl.setRoot(TabsPage);
        },
        error => {
          if(error.statusText == 'Unauthorized'){
            this.myToast.showToaster('E-mail ou senha incorretos.', 5000);
          } else {
            this.myToast.showToaster('Um erro inesperado aconteceu, tente novamente mais tarde.', 5000);
          }
        }
      );
    }

  public goSignup(): void {
    this.navCtrl.push(SignupPage);
  }

  public forgotPassword():void {
    this.alertCtrl.create({
      title: 'Esqueceu sua senha?',
      message: 'Digite o seu e-mail abaixo e dentro de alguns minutos você receberá sua senha.',
      inputs: [
        {
          name: 'email',
          placeholder: 'Seu email',
          type: 'email'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Ok',
          handler : data => {
            console.log(`Enviar e-mail para ${data.email}`);
          }
        }
      ]
    }).present();
  }

  private presentLoading(): Loading {
    let loading = this.loadingCtrl.create({
      content: "Aguarde..."
    });
    loading.present();
    return loading;
  }

}
