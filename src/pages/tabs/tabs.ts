import { Component } from '@angular/core';

import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { AccountPage } from './../account/account';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  public tab1Root = HomePage;
  public tab2Root = ContactPage;
  public tab3Root = AccountPage;
  constructor() {

  }
}
