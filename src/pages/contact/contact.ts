import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ChatPage } from './../chat/chat';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  constructor(public navCtrl: NavController) {

  }

  public goChat(): void {
    this.navCtrl.push(ChatPage);
  }

}
