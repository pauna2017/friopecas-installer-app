import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavController, NavParams, ActionSheetController, LoadingController, ToastController, AlertController, Loading } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthService } from './../../providers/auth-service';
import { MyToast } from './../../providers/mytoast';
import { TabsPage } from './../tabs/tabs';
import { InstallersService } from './../../providers/installers-service';

/*
  Generated class for the FinishSignup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-finish-signup',
  templateUrl: 'finish-signup.html'
})
export class FinishSignupPage {

  public installer: any;
  public firstName: string;
  public lastName: string;

  public photoBase64: string;
  public signupForm: FormGroup;
  public submitted: boolean;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private alertCtrl: AlertController,
    private camera: Camera,
    private installersService: InstallersService,
    private authService: AuthService,
    private myToast: MyToast) {
      this.initForm();
    }

    private setFormValues(): void {
      this.signupForm.get('firstName').setValue(this.firstName);
      this.signupForm.get('lastName').setValue(this.lastName);
      this.signupForm.get('city').setValue(this.installer.cidade);
      this.signupForm.get('state').setValue(this.installer.estado);
      this.signupForm.get('zipCode').setValue(this.installer.numcep);
      this.signupForm.get('codigoFp').setValue(this.installer.codigo);
    }

    private initForm(): void {
      this.signupForm = this.formBuilder.group({
        firstName: ['', [Validators.required, Validators.minLength(3)]],
        lastName: ['', [Validators.required, Validators.minLength(3)]],
        password: ['', [Validators.required, Validators.minLength(3)]],
        email: ['', [Validators.required]],
        photo: ['', [Validators.required]],
        zipCode: ['', [Validators.required]],
        codigoFp: ['', [Validators.required]],
        city: ['', [Validators.required]],
        state: ['', [Validators.required]],
        role: ['installer']
      });
    }

  public getPhoto(): void {
    this.actionSheetCtrl.create({
      title: 'O que deseja?',
      buttons: [
        {
          text: 'Utilizar a câmera',
          icon: 'camera',
          handler: () => {this._getPhoto(this.camera.PictureSourceType.CAMERA)},
        },
        {
          text: 'Selecionar do álbum',
          icon: 'images',
          handler: () => {this._getPhoto(this.camera.PictureSourceType.PHOTOLIBRARY)}
        }
      ]
    }).present();
  }

   private _getPhoto(sourceType): void{
    const options: CameraOptions = {
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      cameraDirection: 1,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      targetHeight: 250,
      targetWidth: 250,
      allowEdit: true,
      sourceType: sourceType
    }
    this.camera.getPicture(options).then(imageData => {
      console.log('imageData', imageData);
      this.photoBase64 = 'data:image/jpeg;base64,' + imageData;
      this.signupForm.get('photo').setValue(imageData);
    }, err => {
      
    });
  }

  public signup(formValue: any, valid: boolean): void {
    if(!formValue.photo){
      this._alertPhoto(formValue);
    } else {
      this._signup(formValue);  
    }
  }

  private _signup(clientToRegister): void {
    this.submitted = true;
    const loading = this.presentLoaderAndToastWelcome();
    this.installersService.register(clientToRegister)
      .finally(() => {
        loading.dismiss();
      })
      .map(res => res.json())
      .subscribe(
        data => {
          console.log('data', data);
          if(data.installer){
            this.authService.login(data);
            this.myToast.showToaster('Bem vindo ao aplicativo de instaladores FrioPeças.', 3000);
            this.navCtrl.setRoot(TabsPage);
          }
        },
        error => {
          let msg;
          if( error.code = 11000 ) {
            msg = 'Desculpe, este perfil já está cadastrado. Em caso de problemas entre em contato com a FrioPeças.'
          } else {
            error = JSON.parse(error['_body']).error;
            switch(error['title']) {
              case 'EmailAlreadyExists':
                msg = 'Este email já está sendo utilizado.';
                break;
              default:
                msg = 'Um erro inesperado aconteceu.'
                break;
            }
          }         
         
          this.alertCtrl.create({
            title: 'Desculpe...',
            message: msg,
            buttons: [{text: 'Ok', role: 'cancel'}]
          }).present();
        }
      );
  }

  private _alertPhoto(formValue: any): void{
    this.alertCtrl.create({
      title: 'Tem certeza?',
      message: 'Você ainda não inseriu uma foto. Tem certeza que deseja concluir o seu cadastro sem adicionar uma foto?',
      buttons: [{
        text: 'Não',
        handler: () => {
          this.getPhoto();
        }
      },
      {
        text: 'Sim',
        role: 'cancel',
        handler: () => {          
          this._signup(formValue);
        }
      }]
    }).present();
  }
  
  public presentLoaderAndToastWelcome(): Loading{
    const loader = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Registrando'
    });
    loader.present();     
    return loader; 
  }

  ionViewDidLoad() {    
    this.initForm();
    this.installer = this.navParams.get('installer');
    let installerName = this.installer.nome.split(' ');
    this.firstName = installerName[0];
    installerName.shift();
    this.lastName = installerName.join(' ');
    this.setFormValues();  
  }

}
