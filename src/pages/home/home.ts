import { UserLogin } from './../../models/userLogin';
import { AuthService } from './../../providers/auth-service';
import { NewVisitPage } from './../new-visit/new-visit';
import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';

import { VisitsService } from './../../providers/visits-service';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public visits: any[];
  public loaded: boolean;
  public userLogin: UserLogin;

  constructor(
    private navCtrl: NavController,
    private modalCtrl: ModalController,
    private visitsService: VisitsService,
    private authService: AuthService,
    private launchNavigator: LaunchNavigator) {
      this.getVisits();
  }

  public getVisits(): void {
    this.authService.getToken().then(
      data =>  data
    ).then(data => {
      this.visitsService.getVisits(data)
        .map(data => data.json())
        .finally(() => this.loaded = true)
        .subscribe(
          res => {
            console.log('res', res);
            this.visits = res;            
          },
          err => {
            console.log('err get visits', err);
          }
        )
    });
   }

   public launchNavigate(address): void {
     this.launchNavigator.navigate(`${address.street} ${address.streetNumber}`)
        .then(
          success => console.log('Launched navigator'),
          error => console.log('Error launching navigator', error)
        );
   }

  public goNewDate(): void {
    const modalNewDate = this.modalCtrl.create(NewVisitPage)
    modalNewDate.present();
    modalNewDate.onDidDismiss(data => {
      this.getVisits();
    });
  }

}
