import { UserLogin } from './../../models/userLogin';
import { AuthService } from './../../providers/auth-service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';

import { VisitsService } from './../../providers/visits-service';
/*
  Generated class for the NewVisit page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-new-visit',
  templateUrl: 'new-visit.html'
})
export class NewVisitPage {

  public mydate;
  public myhour;
  public newVisitForm: FormGroup;
  public submitted: boolean;
  public userLogin: UserLogin;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private visitsService: VisitsService,
    private authService: AuthService,
    private alertCtrl: AlertController) {
      this.authService.getToken().then(
        data =>  this.userLogin = data
      )
      this.initForm();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewVisitPage');
  }
  
  public dismiss(data: any): void {
    this.viewCtrl.dismiss(data);
  }

  private initForm(): void {
    this.newVisitForm = this.formBuilder.group({
      dateHour: ['', [Validators.required]],
      dateDay: ['', [Validators.required]],
      clientName: ['', [Validators.required]],
      description: ['', [Validators.required]],
      address: this.formBuilder.group({
        street: ['', [Validators.required]],
        streetNumber: ['', [Validators.required]],
        zipCode: ['', [Validators.required]]
      })
    });
  }

  public saveVisit(value, valid): void {
    const loader = this.loadingCtrl.create({
      spinner: 'circles',
      content: 'Salvando compromisso'
    });
    loader.present();
    
    value.date = new Date(`${value.dateDay} ${value.dateHour}`);
    delete value.dateDay;
    delete value.dateHour;    
    this.visitsService.createVisit(this.userLogin, value)
      .finally(() => loader.dismiss())
      .subscribe(
        data => {
          this.dismiss(data);
        },
        error => {
          this.alertCtrl.create({
            title: 'Oops..',
            message: 'Certifique-se de ter preenchido todos os campos corretamente.',
            buttons: [{text: 'Ok'}]
          }).present();
        });      
  }
}
