import { FinishSignupPage } from './../finish-signup/finish-signup';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { InstallersService } from './../../providers/installers-service';

/*
  Generated class for the Signup page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage{

  public searchName: string;
  private timeOutSearch: any;

  public searchedOnce: boolean = false;
  public isLoading: boolean;
  public installers = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private installersService: InstallersService) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  public finishSignup(installer): void {
    this.navCtrl.push(FinishSignupPage, {
      installer: installer
    });
  }

  public search(): void {
    if(this.searchName){
        const name = this.searchName;
        clearTimeout(this.timeOutSearch);
        this.timeOutSearch = null;
        this.timeOutSearch = setTimeout(() => {
          this._search(name);
        }, 500);
    }
  }

  private _search(name): void {
    this.isLoading = true;
    this.installersService.getInstallersByName(name)
      .finally(() => {
        this.isLoading = false;
      })
      .map(res => res.json())
      .subscribe(
        data => {
          this.installers = data;      
          this.searchedOnce = true;          
        },
        err => {
          console.log(err);
        }
      )
  }

}

