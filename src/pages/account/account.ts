import { UserLogin } from './../../models/userLogin';
import { MyToast } from './../../providers/mytoast';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from './../login/login';
import { AuthService } from './../../providers/auth-service';

/*
  Generated class for the Account page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {

  public user: UserLogin;
  public loading: boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private authService: AuthService,
    private myToast: MyToast) {
      this.authService.getToken().then(
        res => {
          console.log(res);
          this.user = res;          
          this.loading = false;
        }   
      ) 
    }

  ionViewDidLoad() {
        
  }

  public logout(): void{
    this.authService.logout();
    this.myToast.showToaster('Volte sempre', 3000);
    // WorkAround pra não exibir o tab component na página de login etc.
    this.navCtrl.parent.parent.setRoot(LoginPage);
  }

}
